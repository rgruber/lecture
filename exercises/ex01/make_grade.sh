#!/usr/bin/env bash
# File       : make_grade.sh
# Description: Generate your exercise grade
# Copyright 2020 ETH Zurich. All Rights Reserved.
#
# EXAMPLE:
# python grade.py \
#     --question1 0 \
#     --comment1 'Add a comment to justify your score' \
#     --question2 0 \
#     --question2 0 \
#
# FOR HELP:
# python grade.py --help
#
# The script generates a grade.txt file. Submit your grade on Moodle:
# https://moodle-app2.let.ethz.ch/course/view.php?id=13666

# Note: --question1 and --question2 are not graded
python3.6 grade.py \
    --question1 3 \
    --comment1 '1a: correct numbers' \
    --comment1 '1b: completely misunderstood the question, no points' \
    --comment1 '1c: correct numbers' \
    --question2 9 \
    --comment2 '2a: got different observation and therefore wrong explanation' \
    --comment2 '2b: differnts plot(s), but correct observation' \
    --comment2 '2c: differnts plot(s), but deduced nonetheless that column major is faster' \
    --question3 10 \
    --comment3 '3a: got similar results' \
    --comment3 '3b: got similar figure, explained quantitatively' \
    --comment3 '3c: I only missed to mention the prefetcher' \
    --comment3 '3d: argued with cache miss cost'
