#!/bin/bash

gnuplot -e "
set log xy;
set key left bottom;
set xlabel 'Size [kB]';
set ylabel 'Speedup';
set grid;
set terminal pngcairo;
set output 'results.png';
plot 'results.txt' index 0 using 2:5 w l t 'line'
"
