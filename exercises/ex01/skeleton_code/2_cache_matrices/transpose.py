import matplotlib.pyplot as plt

plt.plotfile('transpose_times.txt', (0, 1, 2, 3, 4, 5 ,6 , 7, 8), delimiter=' ', names=('N', 'time_unoptimized', 'block_2', 'block_4', 'block_8', 'block_16', 'block_32', 'block_64', 'block_128'), subplots=False)

plt.savefig('transpose.svg');
plt.show()