#!/bin/bash

Ns=10

echo "N size[kB] row col speedup"
#for N in 2 5 10 50 100 200; do
for N in 2 5 10 50 100 200 500 600 700 800 900 1000 1100 1200 1300 1400 1500 1600 1800 2000 2200 2500 2800 3000 3500 4000 4500 5000 6000 7000 8000 9000 10000; do
    size="$(echo "scale=2; 8*($N^2 + 2*$N)/1000" | bc)"
    echo -n "$N $size "
    ./matrix_vector "$N" "$Ns" | awk '
        $1=="Row"{printf "%s ", substr($8, 1, length($8)-1)}
        $1=="Col"{printf "%s ", substr($8, 1, length($8)-1)}
        $1=="Speedup"{printf "%s\n", substr($2, 1, length($2)-1)}
    '
done