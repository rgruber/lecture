#!/bin/bash

gnuplot -e "
set key left top;
set xlabel 'matrix size';
set ylabel 'time [s]';
set grid;
set terminal pngcairo;
set output 'results2b.png';
plot 'results2b.txt' index 0 using 1:2 w l t 'nonoptimized',\
     'results2b.txt' index 0 using 1:3 w l t 'b=2',\
     'results2b.txt' index 0 using 1:4 w l t 'b=4',\
     'results2b.txt' index 0 using 1:5 w l t 'b=8',\
     'results2b.txt' index 0 using 1:6 w l t 'b=16',\
     'results2b.txt' index 0 using 1:7 w l t 'b=32',\
     'results2b.txt' index 0 using 1:8 w l t 'b=64',\
     'results2b.txt' index 0 using 1:9 w l t 'b=128'
"
