Working with matrices of size 256
---------------------------------------------
Start C=A*B (non optimized).
Done in total    0.1693s  --  average    0.0339s
---------------------------------------------
Start C=A*B (optimized, row major, block size=2).
Done in total    0.1747s  --  average    0.0349s
Start C=A*B (optimized, row major, block size=4).
Done in total    0.1634s  --  average    0.0327s
Start C=A*B (optimized, row major, block size=8).
Done in total    0.1619s  --  average    0.0324s
Start C=A*B (optimized, row major, block size=16).
Done in total    0.1565s  --  average    0.0313s
Start C=A*B (optimized, row major, block size=32).
Done in total    0.1579s  --  average    0.0316s
Start C=A*B (optimized, row major, block size=64).
Done in total    0.1599s  --  average    0.0320s
Start C=A*B (optimized, row major, block size=128).
Done in total    0.1545s  --  average    0.0309s
---------------------------------------------
Start C=A*B (optimized, column major, block size=2).
Done in total    0.0911s  --  average    0.0182s
Start C=A*B (optimized, column major, block size=4).
Done in total    0.0877s  --  average    0.0175s
Start C=A*B (optimized, column major, block size=8).
Done in total    0.0695s  --  average    0.0139s
Start C=A*B (optimized, column major, block size=16).
Done in total    0.0690s  --  average    0.0138s
Start C=A*B (optimized, column major, block size=32).
Done in total    0.0678s  --  average    0.0136s
Start C=A*B (optimized, column major, block size=64).
Done in total    0.0679s  --  average    0.0136s
Start C=A*B (optimized, column major, block size=128).
Done in total    0.0622s  --  average    0.0124s
==================================================
Working with matrices of size 512
---------------------------------------------
Start C=A*B (non optimized).
Done in total    2.3036s  --  average    0.4607s
---------------------------------------------
Start C=A*B (optimized, row major, block size=2).
Done in total    2.7693s  --  average    0.5539s
Start C=A*B (optimized, row major, block size=4).
Done in total    2.6022s  --  average    0.5204s
Start C=A*B (optimized, row major, block size=8).
Done in total    1.9671s  --  average    0.3934s
Start C=A*B (optimized, row major, block size=16).
Done in total    1.9914s  --  average    0.3983s
Start C=A*B (optimized, row major, block size=32).
Done in total    1.8060s  --  average    0.3612s
Start C=A*B (optimized, row major, block size=64).
Done in total    2.0503s  --  average    0.4101s
Start C=A*B (optimized, row major, block size=128).
Done in total    1.9204s  --  average    0.3841s
---------------------------------------------
Start C=A*B (optimized, column major, block size=2).
Done in total    0.7363s  --  average    0.1473s
Start C=A*B (optimized, column major, block size=4).
Done in total    0.7093s  --  average    0.1419s
Start C=A*B (optimized, column major, block size=8).
Done in total    0.5703s  --  average    0.1141s
Start C=A*B (optimized, column major, block size=16).
Done in total    0.5523s  --  average    0.1105s
Start C=A*B (optimized, column major, block size=32).
Done in total    0.5539s  --  average    0.1108s
Start C=A*B (optimized, column major, block size=64).
Done in total    0.5483s  --  average    0.1097s
Start C=A*B (optimized, column major, block size=128).
Done in total    0.4791s  --  average    0.0958s
==================================================
Working with matrices of size 1024
---------------------------------------------
Start C=A*B (non optimized).
Done in total   47.5982s  --  average    9.5196s
---------------------------------------------
Start C=A*B (optimized, row major, block size=2).
Done in total   47.7288s  --  average    9.5458s
Start C=A*B (optimized, row major, block size=4).
Done in total   45.5481s  --  average    9.1096s
Start C=A*B (optimized, row major, block size=8).
Done in total   46.1246s  --  average    9.2249s
Start C=A*B (optimized, row major, block size=16).
Done in total   47.4650s  --  average    9.4930s
Start C=A*B (optimized, row major, block size=32).
Done in total   47.7658s  --  average    9.5532s
Start C=A*B (optimized, row major, block size=64).
Done in total   47.5164s  --  average    9.5033s
Start C=A*B (optimized, row major, block size=128).
Done in total   47.7446s  --  average    9.5489s
---------------------------------------------
Start C=A*B (optimized, column major, block size=2).
Done in total    6.5017s  --  average    1.3003s
Start C=A*B (optimized, column major, block size=4).
Done in total    5.9547s  --  average    1.1909s
Start C=A*B (optimized, column major, block size=8).
Done in total    5.1450s  --  average    1.0290s
Start C=A*B (optimized, column major, block size=16).
Done in total    4.9881s  --  average    0.9976s
Start C=A*B (optimized, column major, block size=32).
Done in total    4.9458s  --  average    0.9892s
Start C=A*B (optimized, column major, block size=64).
Done in total    4.9141s  --  average    0.9828s
Start C=A*B (optimized, column major, block size=128).
Done in total    4.5624s  --  average    0.9125s
==================================================
