#!/bin/bash

gnuplot -e "
set log x 2;
set key left top;
set xlabel 'block size';
set ylabel 'time [s]';
set grid;
set terminal pngcairo;
set output 'results2c_256.png';
plot 'results2c2.txt' index 0 using 1:2 w l t 'row 256x256',\
     'results2c2.txt' index 0 using 1:3 w l t 'col 256x256',\
"

gnuplot -e "
set log x 2;
set key left top;
set xlabel 'block size';
set ylabel 'time [s]';
set grid;
set terminal pngcairo;
set output 'results2c_512.png';
plot 'results2c2.txt' index 1 using 1:2 w l t 'row 512x512',\
     'results2c2.txt' index 1 using 1:3 w l t 'col 512x512',\
"

gnuplot -e "
set log x 2;
set key left top;
set xlabel 'block size';
set ylabel 'time [s]';
set grid;
set terminal pngcairo;
set output 'results2c_1024.png';
plot 'results2c2.txt' index 2 using 1:2 w l t 'row 1024x1024',\
     'results2c2.txt' index 2 using 1:3 w l t 'col 1024x1024',\
"

gnuplot -e "
set log x 2;
set key left top;
set xlabel 'block size';
set ylabel 'time [s]';
set grid;
set terminal pngcairo;
set output 'results2c_2048.png';
plot 'results2c2.txt' index 3 using 1:2 w l t 'row 2048x2048',\
     'results2c2.txt' index 3 using 1:3 w l t 'col 2048x2048',\
"