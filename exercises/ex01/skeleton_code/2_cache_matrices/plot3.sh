#!/bin/bash

gnuplot -e "
set key left top;
set xlabel 'matrix size';
set ylabel 'time [s]';
set grid;
set terminal pngcairo;
set output 'results2crm.png';
plot 'results2c.txt' index 0 using 1:2 w l t 'nonoptimized',\
     'results2c.txt' index 1 using 1:2 w l t 'row b=2',\
     'results2c.txt' index 2 using 1:2 w l t 'row b=4',\
     'results2c.txt' index 3 using 1:2 w l t 'row b=8',\
     'results2c.txt' index 4 using 1:2 w l t 'row b=16',\
     'results2c.txt' index 5 using 1:2 w l t 'row b=32',\
     'results2c.txt' index 6 using 1:2 w l t 'row b=64',\
     'results2c.txt' index 7 using 1:2 w l t 'row b=128',\
"

gnuplot -e "
set key left top;
set xlabel 'matrix size';
set ylabel 'time [s]';
set grid;
set terminal pngcairo;
set output 'results2ccm.png';
plot 'results2c.txt' index 1 using 1:3 w l t 'col b=2',\
     'results2c.txt' index 2 using 1:3 w l t 'col b=4',\
     'results2c.txt' index 3 using 1:3 w l t 'col b=8',\
     'results2c.txt' index 4 using 1:3 w l t 'col b=16',\
     'results2c.txt' index 5 using 1:3 w l t 'col b=32',\
     'results2c.txt' index 6 using 1:3 w l t 'col b=64',\
     'results2c.txt' index 7 using 1:3 w l t 'col b=128',\
"