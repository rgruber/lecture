#include <omp.h>
#include <random>
#include <stdio.h>
#include <stdlib.h>

// Integrand
inline double F(double x, double y)
{
    if (x * x + y * y < 1.) { // inside unit circle
        return 4.;
    }
    return 0.;
}

// Method 0: serial
double C0(size_t n)
{
    // random generator with seed 0
    std::default_random_engine g(0);
    // uniform distribution in [0, 1]
    std::uniform_real_distribution<double> u;

    double s = 0.; // sum
    for (size_t i = 0; i < n; ++i) {
        double x = u(g);
        double y = u(g);
        s += F(x, y);
    }
    return s / n;
}

// Method 1: openmp, no arrays
// TODO: Question 1a.1
double C1(size_t n)
{
    int nthreads = omp_get_max_threads();
    // one random generator for each thread
    std::default_random_engine* engine = new std::default_random_engine[nthreads];
    std::uniform_real_distribution<double> distribution;
    double sum = 0.0;

    // initialize all random engines with a different seed
    for (size_t i = 0; i < nthreads; i++) { 
        engine[i] = std::default_random_engine(i); // use the thread id as seed
    }

    #pragma omp parallel
    {
        // the thread id
        const int tid = omp_get_thread_num();

        #pragma omp for reduction (+:sum)
        for (size_t i = 0; i < n; ++i) {
            double x = distribution(engine[tid]);
            double y = distribution(engine[tid]);
            sum += F(x, y);
        }
    }

    return sum / n;
}

// Method 2, only `omp parallel for reduction`, arrays without padding
// TODO: Question 1a.2
double C2(size_t n)
{
    int nthreads = omp_get_max_threads();
    // one random generator for each thread
    std::default_random_engine* engine = new std::default_random_engine[nthreads];
    std::uniform_real_distribution<double> distribution;
    double sum = 0;
    double partial_sum[nthreads];

    // initialize all random engines with a different seed
    for (size_t i = 0; i < nthreads; i++) { 
        partial_sum[i] = 0.0;
        engine[i] = std::default_random_engine(i); // use the thread id as seed
    }

    #pragma omp parallel for reduction (+:sum)
    for (size_t i = 0; i < n; ++i) {
        int tid = omp_get_thread_num(); // the thread id
        double x = distribution(engine[tid]);
        double y = distribution(engine[tid]);
        partial_sum[tid] += F(x, y);
    }

    sum = 0.0;
    for (size_t i = 0; i < nthreads; ++i) {
        sum += partial_sum[i];
    }

    return sum / n;

    /*
    // random generator with seed 0
    std::default_random_engine g(0);
    // uniform distribution in [0, 1]
    std::uniform_real_distribution<double> u;

    int nthreads;
    #pragma omp parallel
    #pragma omp master
    nthreads = omp_get_max_threads(); //4
    double sum[nthreads] = {0};

    #pragma omp parallel
    {
        // the thread id
        const int tid = omp_get_thread_num();

        #pragma omp for nowait
        for (size_t i = 0; i < n; ++i) {
            double x = u(g);
            double y = u(g);
            sum[tid] += F(x, y);
        }
    }

    for(size_t i=1;i < nthreads; ++i)
        sum[0] += sum[i];

    return sum[0]/n;
    */
}

// Method 3, only `omp parallel for reduction`, arrays with padding
// TODO: Question 1a.3
double C3(size_t n)
{

    int nthreads = omp_get_max_threads();
    // one random generator for each thread
    std::default_random_engine* engine = new std::default_random_engine[nthreads];
    std::uniform_real_distribution<double> distribution;
    double sum = 0;
    int pad = 64/sizeof(double); // cache line size / double size = 8, padding
    double partial_sum[nthreads*pad];

    // initialize all random engines with a different seed
    for (size_t i = 0; i < nthreads; i++) { 
        partial_sum[i*pad] = 0.0;
        engine[i] = std::default_random_engine(i); // use the thread id as seed
    }

    #pragma omp parallel for reduction (+:sum)
    for (size_t i = 0; i < n; ++i) {
        int tid = omp_get_thread_num(); // the thread id
        double x = distribution(engine[tid]);
        double y = distribution(engine[tid]);
        partial_sum[tid*pad] += F(x, y);
    }

    sum = 0.0;
    for (size_t i = 0; i < nthreads; ++i) {
        sum += partial_sum[i*pad];
    }

    return sum / n;


    /*
    // random generator with seed 0
    std::default_random_engine g(0);
    // uniform distribution in [0, 1]
    std::uniform_real_distribution<double> u;

    int nthreads;
    #pragma omp parallel
    #pragma omp master
    nthreads = omp_get_max_threads(); //4

    int pad = 64/sizeof(double); // cache line size / double size = 8, padding
    double sum[nthreads*pad] = {0}; // initialize sum with 0's

    #pragma omp parallel
    {
        // the thread id
        const int tid = omp_get_thread_num();

        #pragma omp for nowait
        for (size_t i = 0; i < n; ++i) {
            double x = u(g);
            double y = u(g);
            sum[pad*tid] += F(x, y);
        }
    }

    for(size_t i=1;i < nthreads; ++i)
        sum[0] += sum[pad*i];

    return sum[0]/n;
    */
}

// Returns integral of F(x,y) over unit square (0 < x < 1, 0 < y < 1).
// n: number of samples
// m: method
double C(size_t n, size_t m)
{
    switch (m) {
    case 0:
        return C0(n);
    case 1:
        return C1(n);
    case 2:
        return C2(n);
    case 3:
        return C3(n);
    default:
        printf("Unknown method '%ld'\n", m);
        abort();
    }
}

int main(int argc, char* argv[])
{
    // default number of samples
    const size_t ndef = 1e8;

    if (argc < 2 || argc > 3 || std::string(argv[1]) == "-h") {
        fprintf(stderr, "usage: %s METHOD [N=%ld]\n", argv[0], ndef);
        fprintf(stderr, "Monte-Carlo integration with N samples.\n\
METHOD:\n\
0: serial\n\
1: openmp, no arrays\n\
2: `omp parallel for reduction`, arrays without padding\n\
3: `omp parallel for reduction`, arrays with padding\n");
        return 1;
    }

    // method
    size_t m = atoi(argv[1]);
    // number of samples
    size_t n = (argc > 2 ? atoi(argv[2]) : ndef);
    // reference solution
    double ref = 3.14159265358979323846;

    double wt0 = omp_get_wtime();
    double res = C(n, m);
    double wt1 = omp_get_wtime();

    printf("res:  %.20f\nref:  %.20f\nerror: %.20e\ntime: %.20f\n", res, ref,
           res - ref, wt1 - wt0);

    return 0;
}
